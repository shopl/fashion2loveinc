$(document).ready(function() {
  featuredCollectionSlider();
})

const featuredCollectionSlider = function() {
  if($(window).width() < 769) {
    $('.featured-collection-products').slick({
      slidesToShow: 2,
      dots: false,
      arrows: true
    });
  }
}